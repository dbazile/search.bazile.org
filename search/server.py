import logging
import time

from flask import Flask, request
from flask_cors import CORS

from search import repo
from search.contexts import blog, portfolio


_time_started = time.time()

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
CORS(app, origins='*', max_age=86400)

repo.pull()


#
# Routes
#

@app.route('/')
@app.route('/health')
def health_check():
    return {
        'health': {
            'service': 'search',
            'uptime': _get_duration(_time_started),
            'commit': repo.get_commit(),
        },
    }


@app.route('/everywhere')
def search_everywhere():
    start = time.time()
    repo.pull_if_needed()
    query, tags_only = _get_criteria()
    blog_results = blog.search(query, tags_only)
    portfolio_results = portfolio.search(query, tags_only)
    return {
        'results': [*blog_results, *portfolio_results],
        'duration': _get_duration(start),
        'commit': repo.get_commit(),
    }


@app.route('/blog')
def search_blog():
    start = time.time()
    repo.pull_if_needed()
    return {
        'results': blog.search(*_get_criteria()),
        'duration': _get_duration(start),
        'commit': repo.get_commit(),
    }


@app.route('/portfolio')
def search_portfolio():
    start = time.time()
    repo.pull_if_needed()
    return {
        'results': portfolio.search(*_get_criteria()),
        'duration': _get_duration(start),
        'commit': repo.get_commit(),
    }


@app.route('/tags')
def tags():
    tags = set()

    if request.args.get('blog', 'true').lower() == 'true':
        tags.update(blog.tags())
    if request.args.get('portfolio', 'false').lower() == 'true':
        tags.update(portfolio.tags())

    return {
        'tags': sorted(tags),
        'commit': repo.get_commit(),
    }


@app.route('/touch')
def touch():
    start = time.time()
    logging.getLogger(__name__).info('Forcing git pull now')
    repo.pull()
    return {
        'duration': _get_duration(start),
        'commit': repo.get_commit(),
    }


#
# Helpers
#

def _get_duration(start_time: float):
    return round(time.time() - start_time, 3)


def _get_criteria():
    query = request.args.get('query', '').strip().lower()
    tags_only = False
    if query.startswith('tagged:'):
        tags_only = True
        query = query[7:]
    return query, tags_only
